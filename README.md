-----
### Introduction

A GNOME Shell extension that adds a wallpaper slideshow and optionally downloads the BING wallpaper of the day.

-----

### Installation

To install the most recent official release: Visit Wallpaper Slideshow on the [Official GNOME Extensions](https://extensions.gnome.org) website.

-----

### Support Wallpaper Slideshow

Wallpaper Slideshow is provided free of charge. If you enjoy using this extension and wish to help support the project, feel free to use the link below!

[![Donate via Paypal](https://gitlab.com/arcmenu/arcmenu-assets/raw/master/images/paypal_donate.png)](https://www.paypal.com/donate/?cmd=_donations&business=53CWA7NR743WC&item_name=Support+Wallpaper+Slideshow&currency_code=USD&source=url)

-----

### Credits:

**@[AndrewZaech](https://gitlab.com/AndrewZaech) - Project Maintainer and Developer**

#### Other contributions via MR:

**@[daPhipz](https://gitlab.com/daPhipz) - !4, !5**

**@[adamthiede](https://gitlab.com/adamthiede) - !8**


#### Translators:

**@[Quentin](https://gitlab.com/quenty_occitania) - Occitan** | **@[daPhipz](https://gitlab.com/daPhipz) - German**

-----
